#!/usr/bin/env python3

# SPDX-License-Identifier: BSD-2-Clause
# SPDX-FileCopyrightText: 2023 Harald Sitter <sitter@kde.org>

import argparse
import glob
import os
import subprocess
import sys
import yaml
from components import CommonUtils

parser = argparse.ArgumentParser(description='Collect crashes from /tmp')
parser.add_argument('--expecting-cores', default=False, required=False, action='store_true')
arguments = parser.parse_args()

configuration = yaml.safe_load(open(os.path.join(CommonUtils.scriptsBaseDirectory(), 'config', 'global.yml')))
if os.path.exists('.kde-ci.yml'):
    local_config = yaml.safe_load(open('.kde-ci.yml'))
    CommonUtils.recursiveUpdate(configuration, local_config)
require_no_crashes = configuration['Options']['require-no-crashes']
exit_code_on_fail = 1 if require_no_crashes else 0

cores = glob.glob('/tmp/core.*')

if arguments.expecting_cores and len(cores) == 0:
    print('Expected core but found none')
    sys.exit(1)

for core in cores:
    # /tmp/core.%E.%p.%s.%t
    _prefix, escaped_executable, pid, signal, timestamp = core.split('.')
    executable = escaped_executable.replace('!', '/')
    subprocess.check_call(['gdb', '--batch', '--eval-command', 'backtrace', '--core', core, executable])

if len(cores) > 0 and not arguments.expecting_cores:
    sys.exit(exit_code_on_fail)
